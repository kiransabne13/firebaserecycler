package me.codeandroid.firebaserecyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView mRecyclerview;
    EditText mEditText;
    Button mButton;
    NumberModel numberModel;
    ArrayList<NumberModel> numberModelArrayList;
    NumberAdapter numberAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        mRecyclerview = (RecyclerView) findViewById(R.id.recyclerView);
        mEditText = (EditText) findViewById(R.id.editText);
        mButton = (Button) findViewById(R.id.button);

        mRecyclerview.setLayoutManager( new LinearLayoutManager(this));
        numberModelArrayList = new ArrayList<NumberModel>();

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputText = mEditText.getText().toString();
                database.child("1").child(inputText).setValue(inputText);

            }
        });

        database.child("1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                numberModelArrayList.clear(); // this clears arraylist for every new changes and loads again
                if (dataSnapshot.exists()){
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        Log.d("Snapshot", snapshot.getValue().toString());
               /*         NumberModel numberModel = snapshot.getValue(NumberModel.class);
                        numberModelArrayList.add(numberModel);*/
                        String number = snapshot.getValue().toString();
                        NumberModel numberModel;
                        numberModel = dataSnapshot.getValue(NumberModel.class);
                        numberModel.setNumber(number);
                        numberModelArrayList.add(numberModel);


                    }
                    numberAdapter = new NumberAdapter(MainActivity.this, numberModelArrayList);
                    mRecyclerview.setAdapter(numberAdapter);
                    numberAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





    }
}
