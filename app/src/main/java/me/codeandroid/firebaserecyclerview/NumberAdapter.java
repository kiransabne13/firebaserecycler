package me.codeandroid.firebaserecyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class NumberAdapter extends RecyclerView.Adapter<NumberAdapter.MyHolder> {

    Context context;
    ArrayList<NumberModel> numberModelArrayList;

    public NumberAdapter(Context c, ArrayList<NumberModel> n){
        this.context = c;
        this.numberModelArrayList = n;

    }

    @NonNull
    @Override
    public NumberAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyHolder(LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NumberAdapter.MyHolder holder, int position) {
        holder.number.setText(numberModelArrayList.get(position).getNumber());
    }

    @Override
    public int getItemCount() {
        return numberModelArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder{
        TextView number;
        public MyHolder(View itemView) {
            super(itemView);

            number = (TextView) itemView.findViewById(R.id.mobileNumber);

        }
    }
}
